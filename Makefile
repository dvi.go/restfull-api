#include .env

PROJECTNAME=$(shell basename "$(PWD)")
PID=/tmp/.$(PROJECTNAME).pid
MAKEFLAGS += --silent


test:
	echo "Test makefile"

install: 
	go get

run:
	@echo "  >  $(PROJECTNAME) is available at $(ADDR)"
	go run main.go

build:
	go build main.go

all:
	all run step by step

help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)""
	@echo
	@cat Makefile | grep ":"
	@echo